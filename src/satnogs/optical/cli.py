#!/usr/bin/python3
from time import sleep

from datetime import datetime, timedelta

from .upload import handle_stvid_observations
from . import settings


def main():
    while True:
        last_start = datetime.now()
        handle_stvid_observations()

        now = datetime.now()
        next_run = last_start + timedelta(seconds=settings.UPLOAD_INTERVAL)
        if now > next_run:
            print('ERROR: Last upload took longer than settings.UPLOAD_INTERVAL')
            settings.UPLOAD_INTERVAL *= 2
            print(f'Adjusting UPLOAD_INTERVAL to {settings.UPLOAD_INTERVAL} seconds.')
            next_run = now + timedelta(seconds=settings.UPLOAD_INTERVAL)

        remaining = (next_run - now).total_seconds()
        print(f'DEBUG: Sleep {remaining:.0f} seconds...')
        sleep(remaining)
