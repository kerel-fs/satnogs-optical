import os
from pathlib import Path
import requests
import shutil
import re

from requests.adapters import HTTPAdapter, Retry

from . import settings


def upload_stvid_observation(session_name, observation_timestamp):
    """
    Upload the data products for the specified observation
    to SatNOGS DB, then delete it or move it to the archive directory.
    """
    filename_diagnostic_image = Path(settings.OBS_DIR) / session_name / f'{observation_timestamp}_0.png'
    filename_data_json = Path(settings.OBS_DIR) / session_name / f'{observation_timestamp}_data.json'

    url = f'{settings.SATNOGS_DB_BASE_URL}api/optical-observations/'
    headers = {'Authorization': 'Token {0}'.format(settings.SATNOGS_DB_API_TOKEN)}

    # TODO: Remove this workaroud once SatNOGS DB can handle the correct timestamp format
    timestamp_str = observation_timestamp[:11] + observation_timestamp[11:].replace('-', '_')

    files = {'image': (f'{timestamp_str}_0.png', open(filename_diagnostic_image, 'rb')),
             'json': (f'{timestamp_str}_data.json', open(filename_data_json, 'rb'))}

    session = requests.Session()
    retries = Retry(total=settings.SATNOGS_DB_API_MAX_RETRIES,
                    backoff_factor=settings.SATNOGS_DB_API_BACKOFF_FACTOR,
                    status_forcelist=[500])
    session.mount(settings.SATNOGS_DB_BASE_URL, HTTPAdapter(max_retries=retries))

    try:
        if not settings.DEBUG_SKIP_UPLOAD:
            response = requests.post(url,
                                    headers=headers,
                                    files=files,
                                    stream=True,
                                    timeout=settings.SATNOGS_DB_API_TIMEOUT)
            response.raise_for_status()
        print(f'INFO: Observation {observation_timestamp} from session {session_name} successfully uploaded.')

        archive_or_delete_stvid_observation(session_name, observation_timestamp)
    except requests.exceptions.ConnectionError as err:
        print(f'ERROR: {err}')
        print('ERROR: Connection Error. '
              'SatNOGS DB or the network connection is down. '
              'Check https://status.libre.space/')
        return
    except requests.exceptions.Timeout:
        print(f'ERROR: Request timeout when uploading observation {observation_timestamp} to {url}')
    except requests.exceptions.HTTPError as err:
        if response.status_code in [404, 405]: # Client Error
            print(f'ERROR: While uploading observations, the following error occured: {err}\n'
                  'Please report this as bug in satnogs-optical!')
        elif response.status_code == 500: # Server Error
            print(f'ERROR: While uploading results, the following error occured: {err}')
        else:
            print(f'ERROR: While uploading results, the request failed with unexpected HTTP status code {response.status_code}')


def archive_or_delete_stvid_observation(session_name, observation_timestamp):
    """
    Archive or delete the specified stvid observation results.

    To prevent re-processing by stvid the fits file is handled first.
    """
    obs_session_path = Path(settings.OBS_DIR).joinpath(session_name)
    obs_session_archive_path = Path(settings.OBS_ARCHIVE_DIR).joinpath(session_name)

    filename_fits = obs_session_path / (observation_timestamp + '.fits')

    if settings.OBS_ARCHIVE_ENABLED:
        # Move all observation results to the archive directory.

        # Make sure the session directory exists
        obs_session_archive_path.mkdir(exist_ok=True)

        archive_filename_fits = obs_session_archive_path / (observation_timestamp + '.fits')
        shutil.move(filename_fits, archive_filename_fits)

        for filename in obs_session_path.glob(f'{observation_timestamp}*'):
            archive_filename = obs_session_archive_path / filename.name
            shutil.move(filename, archive_filename)
    else:
        # Remove all observation results
        filename_fits.unlink()
        for filename in obs_session_path.glob(f'{observation_timestamp}*'):
            filename.unlink()


def handle_stvid_observations():
    """
    Scan the STVID observation directory for new processed observations.

    New processed observations are idenfitied by their metadata json file.

    If found, upload the following data products:
    - observation metadata json
    - observation diagnostic plot png

    After a successful upload, either delete all products for those
    observations or move them to the archive directory.
    """
    # Detect sessions
    sessions = []
    for item in Path(settings.OBS_DIR).iterdir():
        match = re.match(r'\d{8}_\d', item.name)
        if not match:
            continue
        session_name = item.name

        for itemx in (Path(settings.OBS_DIR) / session_name).iterdir():
            matchx = re.match(r'\d{6}', item.name)
            if not matchx:
                continue
            sessionx_name = itemx.name

            sessions.append(item.name + '/' + itemx.name)

    if not sessions:
        print('DEBUG: No STVID Observations found.')
        return

    print(f'DEBUG: STVID sessions found: {sessions}')

    for session_name in sessions:
        obs_session_path = Path(settings.OBS_DIR) / session_name
        for data_filename in list(obs_session_path.glob('*_data.json')):
            observation_timestamp = data_filename.name[:23]
            upload_stvid_observation(session_name, observation_timestamp)

        # TODO: Find a heuristic when to delete session dirs.
        # Right now this will break any simultaneously running stvid acquire or process.

        # Remove the entier session directory
        # (usually to be called for session of the previous day).
        print(f'INFO: Clean-up session dir {session_name}...')
        for filename in settings.IGNORE_FILES:
            file = obs_session_path / filename
            if file.exists():
                file.unlink()

        # Check for unaccounted files in the session directory
        # NOTE:
        # Doing this instead of deleting them without feedback is a rather defensive approach,
        # to catch errors during this first phase of the development.
        # In the future we might consider deleting session dirs unconditionally.
        remaining_files = list(obs_session_path.glob('*'))
        if len(remaining_files) != 0:
            print(f"WARNING: Can't remove session dir {session_name}, not empty.")
            continue

        # Delete the session directory
        obs_session_path.rmdir()
        print(f'INFO: Session directory {obs_session_path.name} deleted.')
    print('DEBUG: Done.')
