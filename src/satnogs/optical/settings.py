import os

# Load Settings from Environment Variables
OBS_DIR = os.environ.get('OBS_DIR',
                         default='/var/lib/stvid/obs')
SATNOGS_DB_API_TOKEN = os.environ.get('SATNOGS_DB_API_TOKEN',
                                      default='')
SATNOGS_DB_BASE_URL = os.environ.get('SATNOGS_DB_BASE_URL',
                                    default='https://db.satnogs.org/')
SATNOGS_DB_API_TIMEOUT = int(os.environ.get('SATNOGS_DB_API_TIMEOUT',
                                        default='30')) # in integer seconds
SATNOGS_DB_API_MAX_RETRIES = 5 # Total number of retries to allow
SATNOGS_DB_API_BACKOFF_FACTOR = 0.1 # A backoff factor to apply between attempts
OBS_ARCHIVE_ENABLED = (os.environ.get('OBS_ARCHIVE_ENABLED',
                                     default='True') == 'True')
OBS_ARCHIVE_DIR = os.environ.get('OBS_ARCHIVE_DIR',
                                 default='/var/lib/stvid/obs_archive')
DEBUG_SKIP_UPLOAD = True
IGNORE_FILES = ['test.fits', 'position.txt']
UPLOAD_INTERVAL = float(os.environ.get('UPLOAD_INTERVAL', default='120')) # in seconds

# Basic input validation on settings
assert(SATNOGS_DB_API_TOKEN)
assert(SATNOGS_DB_BASE_URL[-1] == '/')
assert(SATNOGS_DB_API_TIMEOUT != 0)
