# satnogs-optical

SatNOGS Optical Ground Station Client software.

## Join

[![irc](https://img.shields.io/badge/Matrix-%23satnogs:matrix.org-blue.svg)](https://app.element.io/#/room/#satnogs:matrix.org)
[![irc](https://img.shields.io/badge/IRC-%23satnogs%20on%20freenode-blue.svg)](https://web.libera.chat/?channels=satnogs)
[![irc](https://img.shields.io/badge/forum-discourse-blue.svg)](https://community.libre.space/c/satnogs)

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202023-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
