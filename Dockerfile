FROM python:3-slim-buster

# Ensure python output appears swiftly in logs
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/local/src/satnogs-optical

COPY . .

RUN pip install -r ./requirements.txt -e .

CMD ["satnogs-optical"]
